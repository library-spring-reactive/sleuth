package com.gitlab.residwi.library.sleuth.baggage;

import brave.baggage.BaggageField;
import brave.baggage.BaggagePropagation;
import brave.baggage.BaggagePropagationConfig;
import brave.baggage.BaggagePropagationCustomizer;
import com.gitlab.residwi.library.sleuth.SleuthAutoConfiguration;
import com.gitlab.residwi.library.sleuth.fields.SleuthExtraFields;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class SleuthExtraFieldCustomizer implements BaggagePropagationCustomizer {

    private final List<SleuthExtraFields> sleuthExtraFields;

    @Override
    public void customize(BaggagePropagation.FactoryBuilder factoryBuilder) {
        sleuthExtraFields.forEach(
                fields -> fields.getFields()
                        .forEach(field -> factoryBuilder.add(BaggagePropagationConfig.SingleBaggageField.newBuilder(BaggageField.create(field))
                                .addKeyName(SleuthAutoConfiguration.HTTP_BAGGAGE_PREFIX + field) /* for HTTP */
                                .addKeyName(SleuthAutoConfiguration.MESSAGING_BAGGAGE_PREFIX + field) /* for Messaging */
                                .build()))
        );
    }
}
