package com.gitlab.residwi.library.sleuth.fields;

import java.util.List;

public interface SleuthExtraFields {

    List<String> getFields();
}
