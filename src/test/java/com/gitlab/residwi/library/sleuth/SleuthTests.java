package com.gitlab.residwi.library.sleuth;

import brave.Span;
import brave.Tracer;
import brave.baggage.BaggageField;
import com.gitlab.residwi.library.sleuth.fields.SleuthExtraFields;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(classes = SleuthTests.Application.class)
class SleuthTests {

    @Autowired
    private Tracer tracer;

    @Test
    void testExists() {
        var span = tracer.newTrace();

        BaggageField.getByName(span.context(), "Resi").updateValue(span.context(), "Value");
        BaggageField.getByName(span.context(), "Dwi").updateValue(span.context(), "Value");
        BaggageField.getByName(span.context(), "Thawasa").updateValue(span.context(), "Value");
        BaggageField.getByName(span.context(), "Hello").updateValue(span.context(), "Value");
        BaggageField.getByName(span.context(), "World").updateValue(span.context(), "Value");

        assertEquals("Value", BaggageField.getByName(span.context(), "Resi").getValue(span.context()));
        assertEquals("Value", BaggageField.getByName(span.context(), "Dwi").getValue(span.context()));
        assertEquals("Value", BaggageField.getByName(span.context(), "Thawasa").getValue(span.context()));
        assertEquals("Value", BaggageField.getByName(span.context(), "Hello").getValue(span.context()));
        assertEquals("Value", BaggageField.getByName(span.context(), "World").getValue(span.context()));
    }

    @Test
    void testNotExists() {
        var span = tracer.newTrace();

        assertNull(BaggageField.getByName(span.context(), "NotExists"));
    }

    @SpringBootApplication
    public static class Application {

        @Component
        public static class HelloSleuthExtraFields implements SleuthExtraFields {

            @Override
            public List<String> getFields() {
                return Collections.singletonList("Hello");
            }
        }

        @Component
        public static class WorldSleuthExtraFields implements SleuthExtraFields {

            @Override
            public List<String> getFields() {
                return Collections.singletonList("World");
            }
        }

    }

}
