# Sleuth Library

Sleuth Library is library to simplify sleuth integration

## Setup

To use this library, setup our pom.xml

```xml
<dependency>
    <groupId>com.gitlab.residwi.library</groupId>
    <artifactId>sleuth</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/11285654/-/packages/maven</url>
    </repository>
</repositories>
```

## Extra Fields

Sleuth support baggage or extra fields. This is field that we can store to sleuth context, and can be accessed anywhere.
Default spring boot already support extra fields using properties, but sometimes we want to add an extra field using
code or in multiple place. Sleuth library support extra fields using spring bean. We only need to create a bean of
class `SleuthExtraFields`

```java
@Component
public static class HelloWorldSleuthExtraFields implements SleuthExtraFields {

    @Override
    public List<String> getFields() {
        return Arrays.asList("Hello", "World");
    }
}

@Component
public static class MandatoryParamSleuthExtraFields implements SleuthExtraFields {

    @Override
    public List<String> getFields() {
        return Arrays.asList("StoreId", "ClientId");
    }
}
``` 

Sleuth library will automatically register all fields, so we can set and get an extra field to sleuth context.

```java
@Autowired
private Tracer tracer;

// set extra field
BaggageField.getByName(span.context(),"Hello").updateValue(span.context(),"Value");
BaggageField.getByName(span.context(),"World").updateValue(span.context(),"Value");
BaggageField.getByName(span.context(),"ClientId").updateValue(span.context(),"Value");
BaggageField.getByName(span.context(),"StoreId").updateValue(span.context(),"Value");

// get extra field
String hello=BaggageField.getByName(span.context(),"Hello").getValue(span.context());
String world=BaggageField.getByName(span.context(),"World").getValue(span.context());
String clientId=BaggageField.getByName(span.context(),"ClientId").getValue(span.context());
String storeId=BaggageField.getByName(span.context(),"StoreId").getValue(span.context());
``` 

## Tracer on Filter

Using Tracer on Filter is not easy. By default, current span is not active on a filter. To simplify this, we can use
class `SleuthFilter` as base interface. The filter method will have `Span` parameter.

```java
@Component
public class ExampleSleuthWebFilter implements SleuthFilter {

    @Autowired
    @Getter
    private Tracer tracer;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain, Span currentSpan) {
        // do something with currentSpan
        BaggageField.getByName(currentSpan.context(), "Key").updateValue(currentSpan.context(), "Value");

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
```